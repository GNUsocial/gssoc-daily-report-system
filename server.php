<?php
$students = [
    'git-nickname',
];
$start_month_offset = 5; // Things started in May
?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<title>Daily Report Viewer | GS SoC</title>
<link rel="icon" href="../../../favicon.png">
<meta charset="utf-8">
<meta name="robots" content="noindex, nofollow, nosnippet">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://hackersatporto.com/assets/css/main.css">
<style>
#side-menu #menu-title {
color: #7a7a7a !important;
font-weight: 700;
}
@media screen and (max-width: 1200px) {
#side-menu #menu-title {
    display:none;
}
}
</style>
</head>
<body>
<header id="header">
<nav id="side-menu">
<label for="show-menu" id="menu-button">Menu</label>
<input id="show-menu" role="button" type="checkbox">
<ul id="menu">
<li><a href="../"><strong>&larr; GS GSoC</strong></a></li>
<li><a href="archive">Previous Months</a></li>
<li id="menu-title">STUDENTS</li>
<?php
    foreach ($students as $s)
    {
        echo '<li><a'.((!empty($_GET['student']) && $s == $_GET['student']) ? ' class="current"' : '').' href="index.php?student='.$s.'">'.$s.'</a></li>';
    }
?>
</ul>
</nav>
<h1>Daily Report Viewer | GS SoC</h1>
<p>Organized by <strong><a href="https://www.diogo.site/">Diogo Peralta Cordeiro</a></strong></p>
<p>Mentors: <a href="https://www.diogo.site/">Diogo Peralta Cordeiro</a>, <a href="https://loadaverage.org/XRevan86">Alexei Sorokin</a>, <a href="https://dansup.com">Daniel Supernault</a>, <a href="http://status.hackerposse.com/rozzin">Joshua Judson Rosen</a> and <a href="https://github.com/phablulo">Phablulo Joel</a></p>
</header>
<?php
if (!empty($_GET['student']) && in_array($_GET['student'], $students))
{
    echo '<article id="student_report">';
    $month = (string)(date('m')-$start_month_offset);
    $nday  = (string)(date('d')-1);
    echo '<h2>Showing reports for '.$_GET['student'].'</h2>';
    $total_hours_this_week = 0;
    for ($i = 0; $i <= $nday ; ++$i)
    {
        echo "<h3 id=\"day-$i\">Day $i</h3>";
        $rand = ($i == $nday) ?? rand(0,31337);
        $report_raw = file_get_contents('https://codeberg.org/GNUsocial/gnu-social-soc-'.date('Y').'-daily-report/raw/master/'.$_GET['student'].'/'.$month.'/'.$i.'?rand='.$rand);
        if ($report_raw)
        {
            $re = '/{[^}]*}/s';
            preg_match_all($re, $report_raw, $matches, PREG_SET_ORDER, 0);
            $report = json_decode($matches[0][0]);
            $report_content_start = strlen($matches[0][0]);
            $report_content = substr($report_raw, $report_content_start);
            $total_hours_this_week += $report->dedicated_hours;
            echo '<p><strong>Summary:</strong> '.$report->summary.'</p>';
            echo '<p><strong>Dedicated Time (in hours):</strong> '.$report->dedicated_hours.'</p>';
            echo '<pre>'.$report_content.'</pre>';
        }
        else
        {
            echo "<p><b>No report.</b></p>";
        }
        // Break weeks
        if (($i+1) % 7 == 0)
        {
            echo "<hr>";
            echo "<p><strong>Total hours this week:</strong> $total_hours_this_week</p>";
            echo "<hr>";
            $total_hours_this_week = 0;
        }
    }
    echo '</article>';
}
else
{
    echo '<h2>Select the student to view a report on the left menu.</h2>
          <article id="time_and_instructions">
            <p>With respect to the time you will have to dedicate, GSoC demands 30h to 40h of work per week. <strong>GNU social\'s Summer of Code expects you to work between [32, 36.5]h/week</strong>, you can organize that time as you please, but you must be sure to dedicate that in your weekly work or to be overly productive.</p>
            <p>We suggest you to do a four-day work week with 6h of work/day + <strong>2h to document, review and test</strong> and report the progress you\'ve done. As breaks are important, we recommend a 1h lunch break, 15min break after 4h of continuous work and a further 15mins break after 6h of work. These breaks won\'t be considered as part of your work time.</p>
            <p>In some places, GSoC starts in the middle of college\'s last exam season, if that\'s your case, we accept that you start doing some hours of work in May (bonding month) and debit those in June (first coding month) weeks.</p>
            <p>In average, you will work 146h/month, ideally 128h/month will be enough (maybe even only 96h/month, if you\'re special). We do not accept that you transfer expected work time from a month to another. <strong>An under-performing week will make us request more hours from you either in the same week or the one immediately after.</strong></p>
            <h2>How to submit a report?</h2>
            <p>Follow the instructions available <a href="https://codeberg.org/GNUsocial/gsoc-daily-report-system">here</a>.</p>
          </article>';
}
?>
</body>
</html>
