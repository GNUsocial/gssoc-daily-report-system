# Daily Reports
## Students

### Setup
1. Clone this repository
2. Fix the first two variables of your copy of the given script as needed
3. Ensure you have the $EDITOR variable set to your preferred text editor

### Usage
1. Run the script,
2. fill in the empty spaces of the shown JSON+HTML snippet and
3. save and close the file.

## VI 101
In case your `$EDITOR` is set to be `vim` (a good choice btw xb):
- Press `i` to enter in insert mode and fill the JSON+HTML snippet
- Press `ESC` followed by `:wq` to write and quit

## Mentors
Daily reports can be read in three different ways:
- [here](https://gnusocial.rocks/soc/current/daily_report/);
- using the `server.php` provided in this repository;
- by reading them directly in this repository.

## LICENSE
All the code is in AGPLv3.
